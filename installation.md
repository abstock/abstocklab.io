## Installation

ABStock est un logiciel libre, il est gratuit à télécharger,
installer, modifier et redistribuer. Vous pouvez aussi passer par nos
services pour qu'on prenne en charge l'installation, l'hébergement et
les mises à jour sur nos serveurs (cf plus bas).

Voici les étapes pour l'installer vous-même.

Installer SBCL:

    apt install sbcl rlwrap

Installer Quicklisp: https://lispcookbook.github.io/cl-cookbook/getting-started.html#install-quicklisp

Il y a une dépendance à cloner dans le répertoire ~/quicklisp/local-projects:

- https://github.com/mmontone/cl-sentry-client

Enfin, cloner le projet:

    git clone https://gitlab.com/vindarel/abstock

## Usage

Pour lancer l'application il y a deux possibilités.

**La première**

    make run
    # aka:
    # rlwrap sbcl --load run.lisp --eval '(in-package :abstock)'

Au premier lancement, ça va installer les dépendances logicielles. Le second démarrage est plus rapide.

Les étapes au lancement sont:

- lecture des variables d'environnement,
- lecture du fichier de config,
- lecture des livres à mettre en avant,
- chargement des données précédemment sauvegardées sur disque, s'il y en a,
- démarrage du server web. À ce stade le site est visible en ligne.
- Enfin, synchronisation avec la base de données (peut prendre 1 à 2 minutes).

Quand l'application est lancée, on obtient un prompt interactif avec lequel on peut interagir avec le site, en live. C'est particulièrement pratique pour recharger la configuration utilisateur. Pour ce faire taper ceci, parenthèses incluses:

    (load-init)

**La seconde** méthode est d'utiliser un exécutable.

Il faut le télécharger (mais il n'est pas encore dispo) ou le construire (voir ci-dessous). Pour le lancer:

    ./abstock

L'avantage est que le démarrage est immédiat, le désavantage est que
l'on perd le prompt interactif (sauf à se connecter via une connexion
Swank).

### Paramètres

Des paramètres sont modifiables via des variables d'environnement:

* le port:

    AB_PORT=9999 sbcl --load run.lisp

* la location du fichier de configuration

    CONFIG=../path/to/config.lisp make run

* ne pas charger la BD (mais lire les données sauvées en fichiers):

    LOAD_DB=nil …


## Configurer

Les textes et autres informations personnelles à afficher sont à configurer dans le
fichier `config.lisp`.

Cf tous les paramètres dans `config-example.lisp`.

Configurez vos paramètres de mail.

~~~lisp
(setf *port* 9889)

(setf *contact-infos*
  '(:|phone| "06 09 09 09 09"
    :|email| "me@test.fr"))

;; SendGrid config:
(setf *email-config*
   '(:|sender-api-key| "api-key"
     :|from| "your@mail.com"))

;; Simple anti-script-kiddy question for the validation form:
(setf *secret-question* "stupid question")
(setf *secret-answer* "answer")

;; Récupérer le résumé du livre lorsqu'on visite sa page ?
(setf *fetch-summaries* t)
~~~

Voyez aussi le fichier `post-config.lisp`, chargé en dernier, pour
redéfinir tout ce qui n'aurait pas pu l'être jusque là, et mettre en place des tâches régulières pour recharger la base de données.

### Sélection du libraire

Dans `config.lisp`, cherchez la variable `enable-product-selection` et passez-la à `t` (au lieu de `nil`).

Vous pouvez aussi ajouter un texte HTML de présentation (`intro-text`).

La liste des coups de cœur peut être établie depuis Abelujo. En ce
cas, ABStock l'extrait de la base de données, vous n'avez plus rien
d'autre à faire.

La liste peut aussi être lue depuis un fichier CSV. Vous devez placer
un fichier `selection.csv` à la racine du projet (cf
`selection-example.csv`). Chaque ligne doit contenir un ISBN et une
quantité (qui n'est pas importante). Exemple:

```
9782742720682;1
9782373090482;1
```

Ce fichier peut être exporté depuis une liste du logiciel Abelujo.

Attention, seuls les livres qui sont dans un rayon sont affichés.


### Envoi de mails

Les mails de commande sont envoyés avec SendGrid. Il faut créer un compte et renseigner sa clef (api key).

Lors de la commande par le formulaire web, un email est envoyé à la librairie et un email de confirmation est envoyé au client.


### Page d'admin

Cette page d'admin vous permet d'avoir la main sur les textes montrés
sur la page d'accueil: texte de bienvenue, présentation de la
sélection du libraire, texte de corps de page…

On utilise un éditeur de texte riche: [Stylo](https://stylojs.com/).

Au démarrage, l'application vous affiche l'URL de la page d'admin. En
effet, il n'y a pas de nom d'utilisateur ni de mot de passe à gérer,
on accède à cette page via une URL unique générée automatiquement. Vous verrez un message de la forme:

```
################################################################################
# Your admin URL is: /1453ca7c-244e-7890-ad1a-3456-admin
################################################################################
```

Vous pouvez la sauvegarder dans votre fichier de config:

~~~lisp
(setf *admin-uuid* "1453ca7c-244e-7890-ad1a-3456")  ;; sans -admin nor /
~~~

Enfin, choisissez un token pour pouvoir enregistrer les modifications depuis
la page d'admin:

~~~lisp
(setf *api-token* "secret token")
~~~

Vous pouvez générer un token avec cette commande:

    openssl rand -base64 32 | tr -d '\n' ; echo

mais n'importe quelle chaîne de caractères suffira.


## Charger des données

### Avec la base de données d'Abelujo

Si ABStock trouve une base de données SQLite nommée `db.db`, il charge
les livres en mémoire.

### Avec d'autres données

On peut définir d'autres produits à charger dans le catalogue.

Pour le moment sont définis les "chargeurs" suivants:

- un chargeur de fichier txt. Les produits sont à définir dans `cards.txt` à la racine du projet.
- selon les besoins: charger un JSON, un CSV, etc.

Voyez le fichier example `cards-example.txt`. Il contient une suite de
fiches articles dans le format suivant:

```
title: Screwdriver
author:
cover: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.jGOb7dVL1oA9VDzNVPDPpwAAAA%26pid%3DApi&f=1
details-url:
shelf: craftmanship
shelf_id: 97
publisher:
price: 10
likes: 1
```

Le seul champ obligatoire est le titre. On peut ajouter des champs
inconnus par défaut à ABStock, comme `likes`, si l'on souhaite aussi
surcharger des templates pour utiliser cette nouvelle donnée.


## Déployer

L'application lancée comme expliqué ci-dessus est directement visible depuis l'internet.

Vous pouvez y ajouter un proxy Apache ou Nginx.


Il vous faut sûrement un cron job pour copier la BD d'Abelujo. Par exemple, toutes les nuits à 4h:

    crontab -e
    00 4 * * * cp /home/path/to/abelujo/db.db /home/path/to/abstock/ && echo $(date) "Abelujo DB copied to ABStock" >> log.txt || echo "oops could not copy Abelujo DB" $(date) >> log.txt

Ou plus simplement, utilisez un lien symbolique.

    cd abstock ; ln -s path/to/abelujo/db.db .


### Rechargement automatique de la base de données

Voyez le fichier de post-config, qui contient des règles pour recharger la base de données à intervalles réguliers.

Voici un exemple qui recharge la BD à heures données:

~~~lisp
  (cl-cron:make-cron-job #'get-all-cards :minute 30 :hour 4)
  (cl-cron:make-cron-job #'get-all-shelves :minute 30 :hour 4)

  (cl-cron:make-cron-job #'get-all-cards :minute 45 :hour 13)
  (cl-cron:make-cron-job #'get-all-shelves :minute 45 :hour 13)

  (cl-cron:make-cron-job #'get-all-cards :minute 30 :hour 20)
  (cl-cron:make-cron-job #'get-all-shelves :minute 30 :hour 20)
~~~

Voici une règle qui le fait toutes les heures:

~~~lisp
(cl-cron:make-cron-job #'get-all-cards :minute 0)
~~~

Et pour le faire toutes les 6 heures:

~~~lisp
(cl-cron:make-cron-job #'get-all-cards :minute 0 :step-hour 6)
~~~

À la fin du fichier, n'oubliez pas de démarrer le job:

~~~lisp
(cl-cron:start-cron)
(schedule-db-reload)
~~~


### Sentry

Pour utiliser Sentry: mettre son DSN dans `~/.config/abstock/sentry-dsn.txt`. (pour le moment, utiliser de DSN "deprecated").

## Développer

Pour construire l'exécutable:

    make build

Pour lancer l'application dans son éditeur Lisp, il faut "`load`er" le
fichier .asd (C-c C-k avec Slime) et charger l'app avec:

    (ql:quickload :abstock)

Puis la démarrer:

    (in-package :abstock)
    (start)  ; optional :port 9999 argument.

Une référence au language de programmation utilisé à garder sous la main: [https://lispcookbook.github.io/cl-cookbook/](https://lispcookbook.github.io/cl-cookbook/)

On utilise [Bulma](https://bulma.io) CSS.

Vous pouvez créer vos propres thèmes, remplacer chaque template par défaut. Voyez la documentation en anglais.

## Offre d'hébergement

Nous pouvons héberger ces solutions sur nos serveurs. Demandez-nous un
devis, écrivez-nous à `contact@abelujo.cc`. N'hésitez pas pour toute question.

## Nous soutenir

Vous avez réussi à installer ABStock ? Bravo ! Vous pouvez nous
soutenir par un don ponctuel ou régulier. Oui, ça compte ! Merci d'avance.

<a href='https://ko-fi.com/K3K828W0V' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

<a href="https://www.patreon.com/bePatron?u=36390714" data-patreon-widget-type="become-patron-button">Become a Patron!</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>
