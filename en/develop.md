# API

ABStock defines API endpoints, free for the developer to use to create new applications:

- `/api/v1/selection.json`
- `/api/v1/lastcreated.json`
- `/api/v1/search.json?query=`

Their result is cached for a few minutes.

# Create new themes

## Simple themes

You can write and load custom code in the files `src/static/theme.js` and `src/static/theme.css`. You can create a symlink to your files.

Here's an example of rules you might want to override:

```css
body {
  background-color: white;
  color: #222831;  // nearly black
  font-size: 18px;
}

.navbar {
    background-color: #222831;  // black-ish
}

.navbar-item {
    color: white;  // font color of the 3 buttons on the left.
}

.basket .button {
    background-color: #ea62ff;  // pink
    color: white;
}
```


## Color themes with Bulma

You can use Bulma's mechanism: https://bulma.io/documentation/customize/

## Full custom themes

You can override any default template and add new ones.

Create your directory of templates in `src/templates/themes/yourtheme/` and
set the `*theme*` variable to it:

    (setf *theme* "yourtheme")

From now on, you can override:

- `base.html`
- `panier.html` which "includes" `validation-form.html`
- `welcome.html`
- and all… see our `templates/` directory.

On startup, ABStock will load your template instead of the default one if it is present.

### Real world theme example

Here's an example of what is possible:

![](abstock-leblason.png "A custom theme for ABStock")

Hint: you can contact us to work on your own theme.
