
<p>
  <h3 align="center" id="abstock"> ABStock </h3>
  <h2 align="center"> Your books catalogue online </h2>
</p>

<p align="center">
  <a href="https://github.com/vindarel/ABStock"><b>Sources</b></a> |
  <a href="#/en/install"><b>Install</b></a> |
  <a href="https://framasphere.org/people/4ac5fae0bed90133a3ed2a0000053625"><b>Blog</b></a> |
  <a href="https://framavox.org/g/V6oiDr8Y/abelujo"><b>Forum</b></a> |
  <a href="https://www.patreon.com/abelujo"><b> Become a Patron </b></a> |
  <a href="https://liberapay.com/vindarel/donate"><b> Donate on Liberapay </b></a> |
  <a href="../">Français</a>

ABStock was developed during the global lock-down to help a bookshop
keep an activity and a link with its clients. It proved 100%
useful. You can have a site on the same model.

[Install it yourself](/en/install.md) or ask us. Contact us at `contact@abelujo.cc`.

NEW: we also develop custom websites for your bookstore. Here is another demo of what we can do: [https://librairie.abelujo.cc/](https://librairie.abelujo.cc/).

</p>


# Features

The website features those pages, all customizable:

- a welcome screen, with:
  - the bookshop's information,
  - a search form. Visitors can **search by title, authors, publisher, shelf and ISBN(s)**.
  - a pre-selection of the books to showcase,
- a **shopping basket**, for visitors to add books in,
- a confirmation form, which sends the command by **email** to the shop owner
  - this form can be disabled
  - we can add online payment
- a special page to **showcase your books selection**
- a simple **admin page** with a rich text editor to edit the application's texts.

Here's how the default search results look like:

![welcome screen](../search.png "search results")

or, a personalized theme:

![your theme](abstock-leblason.png "Another theme built on-demand")

or again, with our complete solution:

![complete bookstore solution](../demo-librairie.png "Complete website")

ABStock connects by default to the [Abelujo](http://abelujo.cc/en/)
database, but it can also read book descriptions from text files and
other inputs. Abelujo is a free software for bookshops.

You can display other types of products.

ABStock is a security and a booster for your bookstore.


# Demo

See by yourself, try the [online demo](http://demo.abstock.org/).

See also this demo of a more complete website: https://librairie.abelujo.cc/

Interested? Please get in touch.

You can install it and host it yourself, we can host it for you and develop more features.


<a href='https://ko-fi.com/K3K828W0V' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

# Develop

See [here](/en/develop.md).
