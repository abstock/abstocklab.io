<img src="cat-shelf-thumb.jpg" style="border-radius: 50%; max-height: 300px" alt="image Adam Ramirez">

# ABStock <small>0.7</small>

> Your books catalogue online.

- Built for bookshops
- Lightning fast
- Free software, professional support

[Github](https://github.com/vindarel/ABStock)
[Get Started](#abstock)
