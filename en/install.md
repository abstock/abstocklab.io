
## Install

ABStock is *libre* software, you can download it, use it, modify it
and redistribute for free. We can also host it for you. Get in touch!

Here's how to install it on your server.

Install SBCL:

    apt install sbcl rlwrap

Install Quicklisp, as explained here: https://lispcookbook.github.io/cl-cookbook/getting-started.html#install-quicklisp

There is one missing dependency in Quicklisp that you have to clone in ~/quicklisp/local-projects:

- https://github.com/mmontone/cl-sentry-client (it is actually optional, I accept a PR to make it optional in the .asd)

Clone the reposity:

    git clone https://gitlab.com/vindarel/abstock

## Run

There are two possibilities.

The first one is to run the app like this:

    make run
    # aka:
    # rlwrap sbcl --load run.lisp

When it is run for the first time, it will install missing
dependencies. Finally, it will print that the webserver was started
and is listening on a given port.

With that method, we are dropped into the Lisp interactive REPL, so we can
interact with the running application. This is useful to reload settings and
such. You can reload all settings with `(load-init)`.

The second method is to use an executable.

Download/build the binary (see below) and run it:

    ./abstock


You can change setings with those environment variables:

* the application port:

    AB_PORT=9999 sbcl --load run.lisp

* the location of the configuration file:

    CONFIG=../path/to/config.lisp make run


## Configure

You can overwrite various parameters and texts:

- the contact information (two phone numbers, email…)
- your SendGrid API key to send emails
- the simple anti-spam validation form question (and answer)
- the welcome texts
- the shopping basket top text
- etc

See all available settings in `config-example.lisp`.

Write yours in `config.lisp` at the project root.

### Contact, email and validation form


~~~lisp
(setf *port* 9889)

(setf *contact-infos*
  '(:|phone|  "06 09 09 09 09"
    :|phone2| "07 09 09 09 09"
    :|email|  "me@test.fr"))

;; SendGrid config:
(setf *email-config*
   '(:|sender-api-key| "api-key"
     :|from| "your@mail.com"))

;; Simple anti-script-kiddy question for the validation form:
(setf *secret-question* "stupid question")
(setf *secret-answer* "answer")
~~~

Then, see `post-config.lisp`, that is loaded last, to override anything.

### Books selection

To enable the special books selection page, set `enable-product-selection` to t:

~~~lisp
(setf enable-product-selection t)
~~~

If you put a file `selection.csv` at the project root, ABStock will
read the books from it. Otherwise, they are retrieved from the DB (one
can select them from Abelujo).

### Admin page

We use a simple admin page with a rich text editor,
[Stylo](https://stylojs.com/), to edit the texts of the front page.

There is no user account or password, rather at startup the app tells you a secret URL:

```
################################################################################
# Your admin URL is: /1453ca7c-244e-7890-ad1a-3456-admin
################################################################################
```

You can save it in the config:

~~~lisp
(setf *admin-uuid* "1453ca7c-244e-7890-ad1a-3456")  ;; sans -admin nor /
~~~

Lastly, choose a secret token for the admin page to be able to save its edits:

~~~lisp
(setf *api-token* "secret token")
~~~

You can generate a random secrete string with the following command:

    openssl rand -base64 32 | tr -d '\n' ; echo

but any string will do.


## Load data

Abelujo's data is loaded by default, from a `db.db` SQLite database.

You can also define other products in `cards.txt` with a simple
format. See `cards-example.txt`.

```
title: Screwdriver
author:
cover: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.jGOb7dVL1oA9VDzNVPDPpwAAAA%26pid%3DApi&f=1
details-url:
shelf: craftmanship
shelf_id: 97
publisher:
price: 10
likes: 1
```

The only mandatory key it `title`. We can use arbritary keys, such as
`likes`, which is unused by default throughout the application. To use
it you'll have to overwright functions or templates.

## Deploy

Link to another configuration file:

    ln -s ../path/to/user/config-user.lisp config.lisp

Link to Abelujo's db like this.

If needed, see the post-config file for rules to reload the DB periodically.

### Automatically reload the DB (with cron-like jobs)

See the post-config example.

~~~lisp
  (cl-cron:make-cron-job #'get-all-cards :minute 30 :hour 4)
  (cl-cron:make-cron-job #'get-all-shelves :minute 30 :hour 4)
  (cl-cron:make-cron-job #'read-selection :minute 40 :hour 4)

  (cl-cron:make-cron-job #'get-all-cards :minute 45 :hour 13)
  (cl-cron:make-cron-job #'get-all-shelves :minute 45 :hour 13)

  (cl-cron:make-cron-job #'get-all-cards :minute 30 :hour 20)
  (cl-cron:make-cron-job #'get-all-shelves :minute 30 :hour 20)
~~~

This reloads the DB every hour:

~~~lisp
(cl-cron:make-cron-job #'get-all-cards :minute 0)
~~~

And this, every 6 hours:

~~~lisp
(cl-cron:make-cron-job #'get-all-cards :minute 0 :step-hour 6)
~~~

**mandatory**: don't forget to start the job:

~~~lisp
(cl-cron:start-cron)
(schedule-db-reload)
~~~



## Develop

Install as shown above.

To build the binary, do:

    make build

To develop in Slime, `load` the .asd (C-c C-k in Slime) and quickload the application with

    (ql:quickload :abstock)

Start the app:

    (in-package :abstock)
    (start)  ; optional :port 9999 argument.

The UI uses the [Bulma](https://bulma.io) CSS framework.

### Sentry

To use **Sentry**: copy your DSN in ~/.config/abstock/sentry-dsn.txt. (currently, use the "deprecated" DSN).

## Support

Congrats on your fresh install. You can support the development effort with a one-time or a regular donation. It helps. Thanks!

<a href='https://ko-fi.com/K3K828W0V' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

<a href="https://www.patreon.com/bePatron?u=36390714" data-patreon-widget-type="become-patron-button">Become a Patron!</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>
