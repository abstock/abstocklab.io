<img src="cat-shelf-thumb.jpg" style="border-radius: 50%; max-height: 300px" alt="image Adam Ramirez">

# ABStock <small>0.9</small>

> Catalogue et commande en ligne

- Logiciel libre
- Support professionnel

[GitHub](https://github.com/vindarel/abstock)
[En savoir plus](#abstock)
