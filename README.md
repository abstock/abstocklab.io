
<p>
  <h3 align="center" id="abstock" title="Abelujo's Book Stock. Le système anti-crash et accélérateur de librairies"> ABStock </h3>
</p>

<p align="center">
  <a href="#/installation?id=installation"><b>Installation</b></a> |
  <a href="https://framasphere.org/people/4ac5fae0bed90133a3ed2a0000053625"><b>Blog</b></a> |
  <a href="https://framavox.org/g/V6oiDr8Y/abelujo"><b>Forum</b></a> |
  <a href="https://gitlab.com/vindarel/abstock"><b>Sources</b></a> |
  <a href="https://www.patreon.com/abelujo"><b>Nous soutenir</b></a> |
  <a href="#/en/"> English</a>

</p>


ABStock a été développé pendant le confinement pour aider les
librairies à garder une activité et un lien avec leurs clients, et est
utilisé avec succès chez [La Palpitante](http://www.lapalpitante.fr/)
à Mens (38) ou encore à la librairie [Le Blason](http://librairieleblason.com/) à Aix (13),
ainsi que dans des librairies associatives.

Il fonctionne par défaut avec la base de
données du logiciel de gestion de librairies
[Abelujo](abelujo.cc). Abelujo fonctionne avec le FEL à la demande de
Dilicom, mais il possède aussi son propre moteur de recherche bibliographique.

Parce que nous souhaitons faciliter la circulation des livres et de
leurs idées, ce logiciel est publié en tant que **logiciel
libre**. Vous pouvez l'installer vous-même **gratuitement**. Sa
documentation technique est même en français.

Vous pouvez également **souscrire à notre offre d'hébergement**,
nous commander un **thème personnalisé pour site vitrine** dont [voici une démo](https://librairie.abelujo.cc/),
nous [offrir un café](https://ko-fi.com/vindarel) ou [nous soutenir](https://www.patreon.com/abelujo).

La page de résultats de recherche ressemble à ceci:

![welcome screen](search.png "Résultats de recherche ")

Ou bien, avec un thème personnalisé:

![thème personnalisé](en/abstock-leblason.png "Thème personnalisé")

Ou encore, avec notre solution complète:

![solution site librairie complète](demo-librairie.png "Site complet")

## Fonctionnalités

Le site se compose de plusieurs pages, toutes configurables:

- La **page d'accueil**, avec:
  - les informations importantes de la librairie,
  - un formulaire de recherche pour **chercher par titre, auteur, éditeur, rayon et ISBN(s)**.
  - et un échantillon de la sélection du libraire.
- En consultant les résultats, l'internaute peut cliquer sur une icône "+" pour **ajouter des livres à son panier**.
- La page **panier** montre la sélection du client, et propose de remplir un formulaire pour valider la commande.
- Le formulaire envoie la **commande** par courriel au libraire.
- Une page spéciale montre la **sélection du libraire**, livres triés par rayon.

Le site fonctionne sur **téléphones** et tablettes, et vous avez accès
aux **analyses de traffic**.

**Pros**: intégration avec le FEL-search de **Dilicom** ou la recherche **Electre**.

**Vous pouvez avoir un site sur le même modèle**. N'hésitez pas à nous
contacter à `contact@abelujo.cc`.

## Démo

Le mieux est que vous testiez par vous-même, voyez la <a
href="http://demo.abstock.org/">démo en ligne</a>.

*NB: cette démo montre le thème par défaut. ABStock est totalement configurable et thémable.*

## Services

Nous vous proposons:

1. d'installer et d'héberger le catalogue pour vous sur nos serveurs. Il est **prêt à l'emploi**.
2. de vous développer un **site vitrine complet et moderne**, avec votre **identité graphique propre**.

Ce site contiendra:

- une page d'accueil, avec l'intégration des nouveautés et de vos coups de cœur via des widgets dynamiques,
- des pages de contenu: Historique, Contact, etc.
- un espace de publication d'articles
  - lesquels peuvent être automatiquement envoyés aux abonnés de votre infolettre et publiés sur votre compte Facebook.
- un espace de rédaction pour ces pages de contenu, avec un éditeur moderne, une gallerie d'images, etc.
- l'accès au catalogue dès la page d'accueil,
- des boutons de partage, un encart d'inscription à votre lettre d'information.

De plus, le site sera **responsive**, il s'adaptera pour les écrans d'ordinateurs, les tablettes et les téléphones.


## Contact

Vous pouvez nous contacter à `contact@abelujo.cc`.

Recevez des nouvelles de ABStock et de Abelujo en vous inscrivant à notre
infolettre. Le volume ne dépasse pas 4 lettres par an.

<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup" class="text-block text-block-centered">

<form action="//abelujo.us12.list-manage.com/subscribe/post?u=d0e4146dcccb946e4ac7c97a7&amp;id=19ff20256c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
    <span >
    <label for="mce-EMAIL">Votre courriel:  </label>
    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
    </span>
    <span id="mce-responses" class="clear">
        <div class="response" id="mce-error-response" style="display:none"></div>
        <div class="response" id="mce-success-response" style="display:none"></div>
    </span>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d0e4146dcccb946e4ac7c97a7_19ff20256c" tabindex="-1" value=""></div>
    <span class="clear"><input type="submit" value="Inscription" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </span>
</form>
</span>


## Installation

La documentation technique est sur la page [installation](installation.md).

<a href='https://ko-fi.com/K3K828W0V' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
